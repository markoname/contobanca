public class ContoCorrente {
    private double saldo;
    private boolean contoBloccato;

    public ContoCorrente(double saldoIniz) {
        this.saldo = saldoIniz;
        this.contoBloccato = false;
    }

    public double getSaldo() {
        return saldo;
    }

    public boolean isContoBloccato() {
        return contoBloccato;
    }

    public void bloccaConto() {
        this.contoBloccato = true;
        System.out.println("Il conto è stato bloccato su richiesta dell'Autorità giudiziaria.");
    }

    public void prelievo(double importoPrelievo) {
        if (importoPrelievo > 0 && importoPrelievo <= saldo && !contoBloccato) {
            saldo -= importoPrelievo;
            System.out.println("Prelievo avvenuto con successo. Saldo rimanente: " + saldo);
        } else if (contoBloccato) {
            System.out.println("Il conto è bloccato. Impossibile effettuare prelievi.");
        } else {
            System.out.println("Importo non valido o saldo insufficiente.");
        }
    }

    public void deposito(double importoDeposito) {
        if (importoDeposito > 0 && !contoBloccato) {
            saldo += importoDeposito;
            System.out.println("Deposito avvenuto con successo. Nuovo saldo: " + saldo);
        } else if (contoBloccato) {
            System.out.println("Il conto è bloccato. Impossibile effettuare depositi.");
        } else {
            System.out.println("Importo non valido per il deposito.");
        }
    }
}
